<?php
// Require composer autoloader
require_once dirname( __DIR__ , 1 ) . '/vendor/autoload.php';

// Create Router instance
$router = new \Bramus\Router\Router();

$router->get('/', function() { 
    echo file_get_contents('page_index.html'); 
});

// Run it!
$router->run();